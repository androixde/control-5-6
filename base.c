#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>

float desvStd(estudiante curso[]){
 	//se definen variables del tipo de dato float y entero
 	float promg = 0, base, dif1, dif2, desviacion = 0;
 	int var = 0, cont = 0, suma;
 	//while saber la cantidad de alumnos
 	while(curso[var].prom != 0.0){
 		promg += curso[var].prom;
 		var++;
 		cont++;
 	}
 	//se crea condicional para cuando la cantidad de notas es 0
 	if(cont == 0){
 		printf("no se han ingresado notas\n");
 		return 0;
 	}
 	promg = promg/cont;//variable promg para saber promedio general del curso
 	var = 0;
 	//para la desviacion mientras var sea menor al contador se sumaran las diferencias
 	while (var<cont){
 		dif1 = curso[var].prom-promg;
 		dif2 = dif1*dif1;
 		suma += dif2;
 		var++;
 	}
 	//el resultado es dividido en la cantida de alumnos y se saca raiz cuadrada
 	base = suma/cont;
 	desviacion= sqrt(base);
 	return desviacion;// se retorna la desviacion
}

float menor(estudiante curso[]){
	//se definen variables del tipo entero y float
 	float prommenor = 7.0;
 	int var = 0,cont=0;
 	//se cuenta la cantidad de alumnos y se suma a los contador
 	while(strcmp(curso[var].nombre,"\0") != 0){
 		var++;
 		cont++;
	var=0;
 	int alumnos =cont;
 	//se recorre los alumnos anteriormente contados y se saca el menor promedio
 	for(var= 0;var<alumnos;var++){
 		if(prommenor > curso[var].prom && curso[var].prom != 0 ){
 			prommenor = curso[var].prom;
 		}
 	}
 	return prommenor;
}
}

float mayor(estudiante curso[]){
 	//se definen variables de tipo entero y float
 	float prommayor = 1.0;
 	int var = 0;
 	//se cuentan los alumnos
 	while(curso[var].prom != 0){
 		//condicionar para obtener el mayor promedio entre los alumnos
 		if(prommayor<curso[var].prom){
 			prommayor = curso[var].prom;
 		}
 		var++;
 	}
 	return prommayor;
}

void cargarEstudiantes(char path[], estudiante curso[]){
	FILE *file;
	int var = 0;
	if((file=fopen(path,"r"))==NULL){
		printf("error al arbir el archivo");
		exit(0); 
	}
	else{
		printf("Estudiantes: \n");
		while (feof(file) == 0) {
			fscanf(file,"%s %s %s", curso[var].nombre,curso[var].apellidoP, curso[var].apellidoM);
			printf("%s %s %s\n", curso[var].nombre,curso[var].apellidoP, curso[var].apellidoM);
			var++;
		}
		fclose(file);
	} 
}

void registroCurso(estudiante curso[]){
	int var,prom[24],prom_parcial;//se definen variables y arreglos
	for (var=0;var<24;var++){//recorremos la lista con la varible definida anteriormente
		curso[var].prom=0;
		printf("Estudiante %s %s %s:\n",curso[var].nombre,curso[var].apellidoP,curso[var].apellidoM);//es la estructura que tendra la recopilacion de notas y donde estan asignadas
		printf("Nota del proyecto 1: ");
		//a continuacion se pide al usuario que registre cada nota con su respectivo porcentaje 
		scanf("%f",&curso[var].asig_1.proy1);
		prom_parcial=prom_parcial+(curso[var].asig_1.proy1 * 0.20);
		printf("Nota del proyecto 2: ");
		scanf("%f",&curso[var].asig_1.proy2);
		prom_parcial=prom_parcial+(curso[var].asig_1.proy2 * 0.20);
		printf("Nota del proyecto 3: ");
		scanf("%f",&curso[var].asig_1.proy3);
		prom_parcial=prom_parcial+(curso[var].asig_1.proy3 * 0.30);
		printf("Nota del control 1: ");
		scanf ("%f",&curso[var].asig_1.cont1);
		prom_parcial=prom_parcial+(curso[var].asig_1.cont1 * 0.05);
		printf("Nota del control 2: ");
		scanf ("%f",&curso[var].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[var].asig_1.cont2 * 0.05);
		printf("Nota del control 3: ");
		scanf ("%f",&curso[var].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[var].asig_1.cont3 * 0.05);
		printf("Nota del control 4: ");
		scanf ("%f",&curso[var].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[var].asig_1.cont4 * 0.05);
		printf("Nota del control 5: ");
		scanf ("%f",&curso[var].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[var].asig_1.cont5 * 0.05);
		printf("Nota del control 6: ");
		scanf ("%f",&curso[var].asig_1.cont2);
		prom_parcial=prom_parcial+(curso[var].asig_1.cont6 * 0.05);
		prom[var]=prom_parcial;

		}
}

void clasificarEstudiantes(char path[], estudiante curso[]){
	FILE *rep;//se crea un primer puntero para el archivo de alumnos reprobados
 	FILE *aprob;//se crea un segundo puntero para el archivo de alumnos aprobados
 	char lst_nopaso[2048]="",lst_paso[2048]="";//se definen arreglos
 	int var=0,True=1;//se definen variables de tipo entero
 	rep=fopen("pasaron.txt","w");//se abre archivo en modo escritura
 	aprob=fopen("nopasaron.txt","w");//se abre archivo en modo escritura
 	while(curso[var].prom != 0.0){//
 		//se ingresan a la lista los alumnos con promedio menor a 4
 		if (curso[var].prom< 4.0 && curso[var].prom != 0){
 			strcat(lst_nopaso,curso[var].nombre);
 			strcat(lst_nopaso, " ");
 			strcat(lst_nopaso,curso[var].apellidoP);
 			strcat(lst_nopaso, " ");
 			strcat(lst_nopaso, curso[var].apellidoM);
 			strcat(lst_nopaso, " ");
 			fprintf(rep,"%s %2f\n",lst_nopaso,curso[var].prom);
 			strcpy(lst_paso,"");
 		}
 		//se ingresan a la lista los alumnos con promedio superior a 4
 		else{
 			strcat(lst_paso,curso[var].nombre);
 			strcat(lst_paso, " ");
 			strcat(lst_paso,curso[var].apellidoP);
 			strcat(lst_paso, " ");
 			strcat(lst_paso, curso[var].apellidoM);
 			strcat(lst_paso, " ");
 			fprintf(aprob, "%s %2f\n",lst_paso,curso[var].prom);
 			strcpy(lst_nopaso,"");
			}
 		}
 	}

void grabarEstudiantes(char path[], estudiante curso[]){
	FILE *file;//se declara puntero
	if((file=fopen(path,"w"))==NULL){//si el archivo se encuentra vacio entonces hay error
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	//si no esta vacio, registra en el archivo los datos de los estudiantes
	else{
		int var;
		for (var=0; var<24; var++){
			fprintf(file,"%s\t%s\t%s\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t %.1f\n", curso[var].nombre,curso[var].apellidoP, curso[var].apellidoM, curso[var].asig_1.proy1, curso[var].asig_1.proy2, curso[var].asig_1.proy3,curso[var].asig_1.cont1, curso[var].asig_1.cont2, curso[var].asig_1.cont3, curso[var].asig_1.cont4, curso[var].asig_1.cont5, curso[var].asig_1.cont6, curso[var].prom);
		
		}
	} 
	printf("\n Se ha logrado el Registro de estudiantes exitosamente \n \n ");
	fclose(file);//se cierra el archivo
}

void metricasEstudiantes(estudiante curso[]){
	//se usan las funciones mayor,menor y desviacion para completar informacion de la metrica
	float desviacion=desvStd(curso),prom_mayor=mayor(curso),prom_menor=menor(curso);
	printf("\n la desviacion estandar es: %3f\n el mejor promedio fue:%2f\nel menor promedio fue: %2f\n",desviacion,prom_mayor,prom_menor);
}

void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta metricas de disperción, tales como mejor promedio; mas bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}